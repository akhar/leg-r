var keystone = require('keystone'),
    async = require('async'),
    _ = require('underscore');

exports = module.exports = function(req, res) {
  
  var view = new keystone.View(req, res);
  var locals = res.locals;
	var temp = {
			types: [],
			formulas: [],
			atoms: []
		};
  // Init locals
  locals.section = 'price';
  locals.data = {};
	
  //Load all types
  view.on('init', function (next){
    keystone.list('Type').model.find()
      .where('tag', 'quick')
      .exec(function (err, types){
        _.each(types, function (type){
          temp.types.push(type);
        })
    });
    next();
  });

  // Load all publiished taged formulas and related atoms
  view.on('init', function (next){
    keystone.list('Formula').model.find()
      .where('state', 'published')
      .populate('atoms')
      .sort('name')
      .exec(function (err, formulas){
        var dutyAtoms = [];
        _.each(formulas, function (formula){
          _.each(formula.atoms, function (atom){
            dutyAtoms.push(atom);
          })
        });
        temp.formulas = formulas;
        temp.atoms = _.uniq(dutyAtoms);
        next();
      });
  });

  //evaluate formulas with values of atoms of them
  view.on('init', function (next){
    _.each(temp.formulas, function (fullFormula){
        var position = _.indexOf(temp.formulas, fullFormula);
        var formula = fullFormula.value;
      _.each(fullFormula.atoms, function (atom){
        formula = formula.replace(atom.name, atom.value);
      });
      temp.formulas[position].result = eval(formula);
    });
    next();
  });

  //bind up all data
  view.on('init', function (next){
    _.each(temp.types, function (type){
      type.formulas = [];
      type.calculators = [];
      _.each(temp.formulas, function (formula){
        if (type.id == formula.type){
          type.formulas.push(formula);
        }
      });
    });
    locals.data.types = temp.types;
    next()
  });

  // Render the view
  view.render('price');
  
};
