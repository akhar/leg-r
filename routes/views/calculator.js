var keystone = require('keystone'),
    async = require('async'),
    _ = require('underscore');

exports = module.exports = function(req, res) {
  
  var view = new keystone.View(req, res);
  var locals = res.locals;

  // Init locals
  locals.section = 'calc';
  locals.data = {
  	calculators : []
  };
	
  // Load calculator and related atoms and params
  view.on('init', function (next){
    keystone.list('Calculator').model.find()
      .where('state', 'published')
      .populate('atoms')
      .populate('params')
      .exec(function (err, calculators){
				_.each(calculators, function(calculator){ //serealize calculator
					var clearCalc = {};
					clearCalc.name = calculator.name;
					clearCalc.formula = calculator.value;
					clearCalc.params = [];
					clearCalc.atoms = [];
					_.each(calculator.params, function(param){
						var clearParam = {};
						clearParam.name = param.name;
						clearParam.dimension = param.dimension;
						clearParam.max = param.max;
						clearParam.min = param.min;
						clearParam.unit = param.unit;
						clearCalc.params.push(clearParam);
					});	
					_.each(calculator.atoms, function(atom){
						var clearAtom = {};
						clearAtom.name = atom.name;
						clearAtom.value = atom.value;
						clearCalc.atoms.push(clearAtom);
					});
					locals.data.calculators.push(clearCalc);
				});
      });

    next();  
  });
	
  // Render the view
  view.render('calculator');
  
};
