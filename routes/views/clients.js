/**
 * Created by akhar on 24.06.15.
 */
var keystone = require('keystone');

exports = module.exports = function(req, res) {

  var view = new keystone.View(req, res),
    locals = res.locals;

  // Set locals
  locals.section = 'clients';
  
  // Load the clients
  view.query('clients', keystone.list('Clients').model.find());

  // Render the view
  view.render('clients');

};
