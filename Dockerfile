FROM    node:0.12.4

# Bundle app source
RUN     mkdir /app
COPY    ./keystone              /app/keystone
COPY    ./models                /app/models
COPY    ./routes                /app/routes
COPY    ./public                /app/public
COPY    ./templates             /app/templates
COPY    keystone.js             /app/
COPY    .env                    /app/
COPY    package.json            /app/

# Install app dependencies
RUN     cd app && \
        npm install

#Run app
CMD     ["node", "/app/keystone.js"]

#Expose port
EXPOSE  3000
