var keystone = require('keystone'),
  Types = keystone.Field.Types;

/**
 * Calculator Model
 * =============
 */

var Calculator = new keystone.List('Calculator', {
autokey: { from: 'name', path: 'key', unique: true },
label: 'Калькуляторы',
	plural: 'Калькуляторы',
	singular: 'Калькулятор'
});

Calculator.add({
	name: { type: String, required: true },
	value: {type: String},
	state: {type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true },
	publishedDate: {type: Types.Date, index: true, dependsOn: { state: 'published' } },
	atoms: {type: Types.Relationship, ref: 'Atom', many: true},
	params: {type: Types.Relationship, ref: 'Parameter', many: true},
	type: {type: Types.Relationship, ref: 'Type'}
});

Calculator.defaultColumns = 'type, name, value';

Calculator.register();
