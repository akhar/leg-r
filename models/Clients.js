/**
 * Created by akhar on 24.06.15.
 */
var keystone = require('keystone'),
  Types = keystone.Field.Types;

/**
 * Clients logos Model
 * =============
 */

var Clients = new keystone.List('Clients', {
  autokey: { from: 'name', path: 'key', unique: true },
  label: 'Клиенты',
  plural: 'Клиенты',
  singular: 'Клиент'
});

Clients.add({
  name: { type: String, required: true },
  logos: { type: Types.CloudinaryImages }
});

Clients.register();
