var keystone = require('keystone'),
  Types = keystone.Field.Types;

/**
 * Parameter Model
 * =============
 */

var Parameter = new keystone.List('Parameter', {
  autokey: { from: 'name', path: 'key', unique: true },
  label: 'Параметры',
  plural: 'Параметры',
  singular: 'Параметр'
});

Parameter.add({
  name: { type: String, required: true },
  description: {type: String},
  unit: {type: String},
  dimension: {type: Number},
  min: {type: Types.Number},
  max: {type: Types.Number}
});

Parameter.defaultColumns = 'name|20%, unit|15%, dimension|15% description|50%';

Parameter.register();
