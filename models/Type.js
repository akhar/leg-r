var keystone = require('keystone'),
  Types = keystone.Field.Types;

/**
 * Type Model
 * =============
 */

var Type = new keystone.List('Type', {
  autokey: { from: 'title', path: 'key', unique: true },
  label: 'Типы',
  plural: 'Типы',
  singular: 'Тип'
});

Type.add({
  name: {type: String},
  images: {type: Types.CloudinaryImages},
  text: {type: Types.Html, wysiwyg: true},
  tag: {type: Types.Select, options: 'billboard, outdoor, wide, quick', emptyOption: false}
});

Type.relationship({ ref: 'Formula', path: 'type' });

Type.defaultColumns = 'name, text';

Type.register();
