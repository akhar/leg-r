var keystone = require('keystone'),
  Types = keystone.Field.Types;

/**
 * Formula Model
 * =============
 */

var Formula = new keystone.List('Formula', {
  autokey: { from: 'name', path: 'key', unique: true },
  label: 'Формулы',
  plural: 'Формулы',
  singular: 'Формула'
});

Formula.add({
  name: { type: String, required: true },
  value: {type: String},
  state: {type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true },
  publishedDate: {type: Types.Date, index: true, dependsOn: { state: 'published' } },
  atoms: {type: Types.Relationship, ref: 'Atom', many: true},
  type: {type: Types.Relationship, ref: 'Type'}
});

Formula.defaultColumns = 'type, name, value';

Formula.register();
