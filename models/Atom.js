var keystone = require('keystone'),
  Types = keystone.Field.Types;

/**
 * Atomic Model
 * =============
 */

var Atom = new keystone.List('Atom', {
  autokey: { from: 'name', path: 'key', unique: true },
  label: 'Атомы',
  plural: 'Атомы',
  singular: 'Атом'
});

Atom.add({
  name: { type: String, required: true },
  description: {type: String},
  value: {type: Types.Money,  format: '0,0 $', currency: 'ru'}
});

Atom.defaultColumns = 'name|25%, value|15%, description|60%';

Atom.register();
